%global fontname nerd

Name:           %{fontname}-fonts
Version:        3.2.1
Release:        1%{?dist}
Summary:        Nerd Fonts patched with a high number of glyphs

License:        OFL
URL:            https://github.com/ryanoasis/nerd-fonts
Source1:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/FiraCode.zip
Source2:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/FiraMono.zip
Source3:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/Hasklig.zip
Source4:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/JetBrainsMono.zip
Source5:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/RobotoMono.zip
Source6:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/SourceCodePro.zip
Source7:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/Ubuntu.zip
Source8:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/UbuntuMono.zip
Source9:        https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/IntelOneMono.zip
Source10:       https://github.com/ryanoasis/nerd-fonts/releases/download/v%{version}/Monaspace.zip

BuildArch:      noarch
BuildRequires:  fontpackages-devel
# BuildRequires:  libappstream-glib
BuildRequires:  unzip

Requires:       fontpackages-filesystem

%description
Nerd Fonts patch developer targeted fonts with a high number of glyphs (icons).
Specifically to add a high number of extra glyphs from popular ‘iconic fonts’
such as Font Awesome, Devicons, Octicons, and others.


%package -n nerd-fira-code-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-fira-code-fonts
Patched font Fira (Fura) Code from the nerd-fonts library.


%package -n nerd-fira-mono-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-fira-mono-fonts
Patched font Fira Mono from the nerd-fonts library.


%package -n nerd-hasklig-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-hasklig-fonts
Patched font Hasklig from the nerd-fonts library.


%package -n nerd-jetbrains-mono-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-jetbrains-mono-fonts
Patched font JetBrains Mono from the nerd-fonts library.


%package -n nerd-roboto-mono-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-roboto-mono-fonts
Patched font Roboto Mono from the nerd-fonts library.


%package -n nerd-source-code-pro-fonts
Summary: Monospaced font family for user interface and coding environments

Requires:       fontpackages-filesystem

%description -n nerd-source-code-pro-fonts
Patched font SourceCodePro from nerd-fonts library.


%package -n nerd-ubuntu-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-ubuntu-fonts
Patched font Ubuntu from the nerd-fonts library.


%package -n nerd-ubuntu-mono-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-ubuntu-mono-fonts
Patched font Ubuntu Mono from the nerd-fonts library.


%package -n nerd-intel-one-mono-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-intel-one-mono-fonts
Patched font Intel One Mono from the nerd-fonts library.


%package -n nerd-monaspace-fonts
Summary: Free monospaced font with programming ligatures

Requires:       fontpackages-filesystem

%description -n nerd-monaspace-fonts
Patched font Monaspace from the nerd-fonts library.


%prep
rm -rf %{_builddir}/%{name}-%{version}/*
mkdir %{_builddir}/%{name}-%{version}
unzip %{SOURCE1} -d %{_builddir}/%{name}-%{version}/FiraCode
unzip %{SOURCE2} -d %{_builddir}/%{name}-%{version}/FiraMono
unzip %{SOURCE3} -d %{_builddir}/%{name}-%{version}/Hasklig
unzip %{SOURCE4} -d %{_builddir}/%{name}-%{version}/JetBrainsMono
unzip %{SOURCE5} -d %{_builddir}/%{name}-%{version}/RobotoMono
unzip %{SOURCE6} -d %{_builddir}/%{name}-%{version}/SourceCodePro
unzip %{SOURCE7} -d %{_builddir}/%{name}-%{version}/Ubuntu
unzip %{SOURCE8} -d %{_builddir}/%{name}-%{version}/UbuntuMono
unzip %{SOURCE9} -d %{_builddir}/%{name}-%{version}/IntelOneMono
unzip %{SOURCE10} -d %{_builddir}/%{name}-%{version}/Monaspace


%build


%install
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/FiraCode/F[iu]ra*Code*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/FiraMono/F[iu]ra*Mono*.otf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/Hasklig/Hasklug*.otf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/JetBrainsMono/JetBrainsMono*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/RobotoMono/Roboto*Mono*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/SourceCodePro/Sauce*Code*Pro*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/Ubuntu/Ubuntu*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/UbuntuMono/Ubuntu*Mono*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/IntelOneMono/IntoneMono*.ttf %{buildroot}%{_fontdir}
install -m 0644 -p %{_builddir}/%{name}-%{version}/Monaspace/Monaspice*.otf %{buildroot}%{_fontdir}


%files -n nerd-fira-code-fonts
%{_fontdir}/Fira*Code*.ttf


%files -n nerd-fira-mono-fonts
%{_fontdir}/F[iu]ra*Mono*.otf


%files -n nerd-hasklig-fonts
%{_fontdir}/Hasklug*.otf


%files -n nerd-jetbrains-mono-fonts
%{_fontdir}/JetBrains*Mono*.ttf


%files -n nerd-roboto-mono-fonts
%{_fontdir}/Roboto*Mono*.ttf


%files -n nerd-source-code-pro-fonts
%{_fontdir}/Sauce*Code*Pro*.ttf


%files -n nerd-ubuntu-fonts
%{_fontdir}/Ubuntu*.ttf


%files -n nerd-ubuntu-mono-fonts
%{_fontdir}/Ubuntu*Mono*.ttf


%files -n nerd-intel-one-mono-fonts
%{_fontdir}/IntoneMono*.ttf


%files -n nerd-monaspace-fonts
%{_fontdir}/Monaspice*.otf


%changelog
* Mon May 06 2024 Freeman Zhang <zhanggyb@163.com> - 3.2.1-1
- Upgrade to 3.2.1
- Add Monaspace

* Mon Apr 08 2024 Freeman Zhang <zhanggyb@163.com> - 3.2.0-2
- Add IntelOneMono

* Mon Apr 08 2024 Freeman Zhang <zhanggyb@163.com> - 3.2.0-2
- Bump to 3.2.0

* Wed Aug 16 2023 Freeman Zhang <zhanggyb@163.com> - 3.0.2-1
- Upgrade to 3.0.2

* Sat Feb 25 2023 Freeman Zhang <zhanggyb@163.com> - 2.3.3-2
- Add Hasklig, JetBrainsMono

* Tue Feb 21 2023 Freeman Zhang <zhanggyb@163.com> - 2.3.3-1
- Upgrade to 2.3.3
- Add FiraMono, RobotoMono, Ubuntu, UbuntuMono

* Mon Oct 17 2022 Freeman Zhang <zhanggyb@163.com> - 2.2.2-1
- Upgrade to 2.2.2

* Sun May 08 2022 Freeman Zhang <zhanggyb@163.com> - 2.1.0-1
- Initial package provides nurd Fira Code and Source Code Pro
