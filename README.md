# nerd-fonts

[Nerd Fonts](https://github.com/ryanoasis/nerd-fonts) is a project that patches
developer targeted fonts with a high number of glyphs (icons). Specifically to
add a high number of extra glyphs from popular ‘iconic fonts’ such as [Font
Awesome](https://github.com/FortAwesome/Font-Awesome),
[Devicons](https://vorillaz.github.io/devicons/),
[Octicons](https://github.com/primer/octicons), and
[others](https://github.com/ryanoasis/nerd-fonts#glyph-sets).

<p style="text-align:center">
<img src="https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/images/sankey-glyphs-combined-diagram.svg" alt="diagram">
</p>
